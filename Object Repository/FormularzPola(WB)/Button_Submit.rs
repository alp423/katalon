<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk potwierdzający dodanie nowego rzędu do tabeli.</description>
   <name>Button_Submit</name>
   <tag></tag>
   <elementGuidId>c019ebc7-a706-4363-a586-b5df3bc540a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#submit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
