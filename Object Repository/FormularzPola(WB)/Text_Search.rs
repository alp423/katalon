<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole do wyszukiwanie, użyte do potwierdzenia</description>
   <name>Text_Search</name>
   <tag></tag>
   <elementGuidId>6f295348-468a-4ce7-8ec2-024d0f83c91c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#searchBox</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
