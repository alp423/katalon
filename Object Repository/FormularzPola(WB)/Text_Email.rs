<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole tekstowe zawierające adres email.</description>
   <name>Text_Email</name>
   <tag></tag>
   <elementGuidId>500c88a4-fa41-4bdb-a330-3c4716a0aa91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#userEmail</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
