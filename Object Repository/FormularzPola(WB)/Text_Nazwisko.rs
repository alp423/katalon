<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole tekstowe zawierające nazwisko.</description>
   <name>Text_Nazwisko</name>
   <tag></tag>
   <elementGuidId>2584948e-afd9-4f3e-bd97-98054700b0cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#lastName</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
