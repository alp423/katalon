<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole tekstowe zawierające wydział.</description>
   <name>Text_Wydzial</name>
   <tag></tag>
   <elementGuidId>cd80ab4e-ae9f-4fd8-9aed-56b6eb3faf33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#department</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
