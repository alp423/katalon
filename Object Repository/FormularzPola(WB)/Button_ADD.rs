<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk dodania nowego rzędu do tablicy.</description>
   <name>Button_ADD</name>
   <tag></tag>
   <elementGuidId>db5474e9-bcbd-4634-98df-565c2295b1c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#addNewRecordButton</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
