<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Label dla pola radio - oznacza płeć</description>
   <name>Label_Male</name>
   <tag></tag>
   <elementGuidId>43baabee-6298-4160-a2ea-fcd23cc80417</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label.custom-control-label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
