<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Tekst, który pojawia się po poprawnym wysłaniu formularza.</description>
   <name>Text_Success</name>
   <tag></tag>
   <elementGuidId>1d5539ef-a63c-44b6-a501-7f3726907a6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#example-modal-sizes-title-lg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
