<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Label dla pola radio - oznacza płeć</description>
   <name>Label_Female</name>
   <tag></tag>
   <elementGuidId>24d23a66-53e2-4359-af68-2adb4b183da5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.custom-radio:nth-child(2) > label:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
