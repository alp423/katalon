<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Label dla pola radio - oznacza płeć</description>
   <name>Label_Other</name>
   <tag></tag>
   <elementGuidId>72e7a15f-1115-41b8-a7a1-6f8010bfcd68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.custom-radio:nth-child(3) > label:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
