<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole zawierające email</description>
   <name>Input_Email</name>
   <tag></tag>
   <elementGuidId>4212d7ca-7184-4cda-b40d-441e902c27f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[id=&quot;userEmail&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
