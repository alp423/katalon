<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suite do drugiego formularzu - web table.</description>
   <name>TS_Formularz(WB)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>90b979fd-9f2b-416e-82a2-01f7b5e03016</testSuiteGuid>
   <testCaseLink>
      <guid>5018d29e-618a-42ec-8b38-411d6361c28b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Formularz_test/TC_FormularzWB</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f7a6f0fe-9db6-4fa3-9d7b-db486b3b2162</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4414f018-00a0-4ec9-8be3-992a061beb21</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f0be5310-37e6-4274-af7b-c8b09dae6825</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>627b1ffe-8f2a-4360-98b9-1baef6bbef31</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f5dcc52f-4512-426a-a488-65027b43c15d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>708238ba-65fc-4d6d-b888-c720ce45650a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
