import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(rawUrl=GlobalVariable.URL2) //uruchomienie przeglądarki z adresem strony

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/FormularzPola(WB)/Button_ADD'))

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Imie'), this.Imie.trim())

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Nazwisko'), this.Nazwisko.trim())

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Email'), this.Email.trim())

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Wiek'), this.Wiek.trim())

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Salary'), this.Salary.trim())

WebUI.setText(findTestObject('Object Repository/FormularzPola(WB)/Text_Wydzial'), this.Wydzial.trim())

WebUI.click(findTestObject('Object Repository/FormularzPola(WB)/Button_Submit'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('Object Repository/FormularzPola(WB)/Button_Submit'), 1)
// asercja, tylko tutaj strona troche nie pasuje, brak informacji po dodaniu do tabeli, więc sprawdzam czy zniknelo okno rejestracji (po przycisku submit) - znika tylko jesli dane przed Submitem sa poprawne

WebUI.closeBrowser()