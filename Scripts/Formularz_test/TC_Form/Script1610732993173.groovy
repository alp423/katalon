import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.openBrowser(rawUrl=GlobalVariable.URL) //uruchomienie przeglądarki z adresem strony

WebUI.maximizeWindow()

WebUI.setText(findTestObject('FormularzPola/Input_Imie'), this.Imie.trim())

WebUI.setText(findTestObject('FormularzPola/Input_Nazwisko'), this.Nazwisko.trim())

WebUI.setText(findTestObject('FormularzPola/Input_Email'), this.Email.trim())

/* Zależnie od podanej wartosci w pliku wybierz jedna z 3 opcji typu RADIO, trim poniewaz tekst zawiera spacje :(  */
if (this.Plec.trim() == "Male") {
	WebUI.click(findTestObject('Object Repository/FormularzPola/Label_Male'))
} else if (this.Plec.trim() == "Female"){
	WebUI.click(findTestObject('Object Repository/FormularzPola/Label_Female'))
} else {
	WebUI.click(findTestObject('Object Repository/FormularzPola/Label_Other'))
}

WebUI.setText(findTestObject('Object Repository/FormularzPola/Input_NrTelefonu'), this.NrTelefonu.trim())

WebUI.click(findTestObject('Object Repository/FormularzPola/Button_Submit'))

//WebUI.waitForElementPresent(findTestObject('Object Repository/FormularzPola/Text_Success'), 0)  poprzednio używane do asercji
WebUI.verifyElementPresent(findTestObject('Object Repository/FormularzPola/Text_Success'), 1)
// asercja

WebUI.closeBrowser()